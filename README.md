# Cocos2d-x Android Project on Visual Studio #


### What is this repository for? ###

* This repository contains a small template that helps you develop and build Cocos2d-x Android project on Visual Studio.
* This template is based on the template of IanHuff with some improvements.
(https://blogs.msdn.microsoft.com/vcblog/2016/07/12/cocos2d-x-visual-studio-android-project/)
* Version 1.0

### Prequisites: ###

- Cocos2d-x v3.13.1 working environment
- Visual Studio 2015 Update 2

### Installation guide: ###

- Clone this template to your computer and decompress.
- Copy the template (proj.visual) to your project folder, same place with 'proj.android', 'proj.linux' etc..
- Open Terminal or Command promt, navigate to 'proj.visual' folder and run the 'Rename.py' script. This script will rename all relative name in the template to your game name. 
- You might need fix the package name of the game in python script with any text editor. 
- Open the Visual Solution and build. That's it. 
### Notice ###

- The assets will refresh everytime you reload the project, so if you change the assets in resources folder, please unload and reload the project. 
- If you want to create new class under a new subdirectory, please create the Filter first, and then check the dir path when creating. 

### Support me ###

- Support me by playing my games at: https://play.google.com/store/apps/dev?id=7294876713940151420