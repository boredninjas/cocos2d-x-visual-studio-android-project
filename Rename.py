#!/usr/bin/python
# coding=utf-8
import os
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
PROJECT_NAME=os.path.split(os.path.dirname(SITE_ROOT))[1]
PACKAGE_NAME = "com.boredninjas." + PROJECT_NAME
#Edit AndroidManifest
fileout = "HelloCpp/AndroidManifest.xml"
f = open(fileout,'r')
filedata = f.read()
f.close()
changePackage = filedata.replace("com.$(ApplicationName)",PACKAGE_NAME)
f = open(fileout,'w')
f.write(changePackage)
f.close()
#Fixing the project solution
fileout = "HelloCpp.sln"
f = open(fileout,'r')
filedata = f.read()
f.close()

fixsln = filedata.replace("HelloCpp",PROJECT_NAME)
f = open(fileout,'w')
f.write(fixsln)
f.close()
#Fixing the project name
fileout = "HelloCpp/res/values/strings.xml"
f = open(fileout,'r')
filedata = f.read()
f.close()

fixsln = filedata.replace("CocosVisualStudio",PROJECT_NAME)
f = open(fileout,'w')
f.write(fixsln)
f.close()
#Rename in subfolder
os.rename("HelloCpp/HelloCpp.androidproj","HelloCpp/"+PROJECT_NAME+".androidproj")
os.rename("HelloCpp/HelloCpp.androidproj.user","HelloCpp/"+PROJECT_NAME+".androidproj.user")
#Rename parent folder
os.rename("HelloCpp.sln",PROJECT_NAME + ".sln")
os.rename("HelloCpp",PROJECT_NAME)
print("Done editing project name and dependencies")
